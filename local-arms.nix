{ pkgs, ... }:

{
  imports = [
    ./lpmode.nix
    ./fixlockdir.nix
  ];

  environment.systemPackages = with pkgs; [
    gs24dsi
    aiofxload
    aiousb-fw
    iguana-adc
    arms-utils
  ];

  boot.extraModulePackages = with pkgs; [
    gs24dsi
    iguana-adc
  ];

  boot.kernelModules = [
    "24dsi"
    "iguana_adc"
  ];

  boot.extraModprobeConfig = ''
    # Configure GPIO lines 8-11 as outputs.
    options iguana_adc output_mask=0x0f00 enable_pullups=1
  '';

  services.udev.packages = with pkgs; [
    gs24dsi
    aiousb-fw
  ];

  services.udev.extraRules = ''
    # Allow r/w access to AccesIO USB-DA12-8A DAC board
    SUBSYSTEM=="usb", ATTR{idVendor}=="1605", ATTR{idProduct}=="4002", MODE="0666"
  '';

  services.lpmode.enable = true;
  services.fixlockdir.enable = true;
}

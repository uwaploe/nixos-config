# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:
let
  prj = "ivar";
  datamount = null;
  staticip = "10.13.0.10";
in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./localpkgs.nix
      ./packages.nix
      ./loginctl-linger.nix
      ./startchrony.nix
    ] ++ lib.optional (prj == "ivar") ./local-ivar.nix
      ++ lib.optional (prj == "arms") ./local-arms.nix;

  fileSystems = lib.mkIf (datamount != null) {
    "${datamount}" = {
      device = "/dev/disk/by-label/${prj}data";
      fsType = "ext4";
    };
  };

  boot.kernelPackages = pkgs.linuxPackages;
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";
  # Enable serial console for GRUB
  boot.loader.grub.extraConfig = "serial --speed=115200 --unit=0; terminal_input --append serial; terminal_output --append serial";

  # Enable serial console for Linux
  boot.kernelParams = ["console=tty1" "console=ttyS0,115200n8"];

  # Sysctl settings
  boot.kernel.sysctl."kernel.panic" = 30;

  networking.hostName = "${prj}-sic-1"; # Define your hostname.
  # Static address for second interface
  networking.interfaces.enp2s0.ip4 = [{ address = "${staticip}"; prefixLength = 24; }];
  networking.firewall.enable = false;

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "UTC";
  services.ntp.enable = false;
  services.chrony.enable = true;
  services.chrony.servers = [
    "time1.u.washington.edu offline"
  ];
  # This service will check for an internet connection and,
  # if it's available, switch chronyd to online mode.
  services.startchrony.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable Mosquitto MQTT broker
  services.mosquitto.enable = true;
  services.mosquitto.allowAnonymous = true;
  services.mosquitto.users = { };

  # SMB file sharing
  services.samba.enable = true;
  services.samba.syncPasswordsByPam = true;
  services.samba.shares = lib.mkIf (datamount != null) {
    ivar = {
      path = "${datamount}";
      "read only" = "yes";
      browseable = "yes";
      "write list" = "sysop";
      "create mask" = 0644;
      "directory mask" = 0755;
    };
  };
  services.samba.extraConfig = ''
    workgroup = ${lib.toUpper prj}
    hosts allow = 127.0.0.1 10.95.97.0/24 10.13.0.0/24 10.0.97.0/24
  '';

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.sysop = {
    isNormalUser = true;
    uid = 1000;
    description = "System Operator";
    extraGroups = ["wheel" "users" "dialout"];
    linger = true;
  };

  # Allow the sysop account to use real-time scheduling priorities.
  security.pam.loginLimits = [
    { domain = "sysop"; item = "rtprio"; type = "hard"; value = "99"; }
    { domain = "sysop"; item = "rtprio"; type = "soft"; value = "99"; }
  ];

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  nix.trustedUsers = ["root" "sysop"];

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "16.03";

}

{ config, lib, pkgs, ... }:
let
  cfg = config.services.lpmode;
in {
  options = {
    services.lpmode = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to enable `lpmode' service.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pkgs.arms-utils ];
    services.mosquitto.enable = lib.mkDefault true;

    systemd.services.lpmode = {
      description = "Put CPU into a low-power sleep mode on command";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "mosquitto.service" ];
      path = [ pkgs.mosquitto pkgs.coreutils pkgs.eject pkgs.kmod ];
      serviceConfig = {
        Type = "simple";
        ExecStart = ''
          ${pkgs.arms-utils}/sbin/lpmode
        '';
        Restart = "always";
      };
    };
  };
}

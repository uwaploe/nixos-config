{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    wget
    chrony
    which
    emacs25-nox
    python27Full
    python27Packages.pip
    python27Packages.virtualenvwrapper
    tmux
    fping
    gitAndTools.gitFull
    socat
    gcc5
    gnumake
    libusb1
    libcap_progs
    usbutils
    pciutils
    mosquitto
    kermit
    attr
    samba
    lsof
  ];

}

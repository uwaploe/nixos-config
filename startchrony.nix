{ config, lib, pkgs, ... }:
let
  cfg = config.services.startchrony;
in {
  options = {
    services.startchrony = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to enable `startchrony' service.
        '';
      };

      ipaddr = lib.mkOption {
        type = lib.types.string;
        default = "140.142.1.8";
        description = ''
          IP address to ping to check for Internet connectivity.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pkgs.chrony pkgs.fping ];
    services.chrony.enable = lib.mkDefault true;

    systemd.services.startchrony = {
      description = "Put chronyd in online mode if network is up";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "chronyd.service" ];
      path = [ pkgs.chrony pkgs.fping ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = ''
          ${pkgs.fping}/bin/fping ${cfg.ipaddr} ; ${pkgs.chrony}/bin/chronyc online
        '';
        Restart = "no";
      };
    };
  };
}

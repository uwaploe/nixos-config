{ pkgs, ... }:

{
    imports = [
    ./fixlockdir.nix
  ];

  environment.systemPackages = with pkgs; [
    gs24dsi
    iguana-adc
  ];

  boot.extraModulePackages = with pkgs; [
    gs24dsi
    iguana-adc
  ];

  boot.kernelModules = [
    "24dsi"
    "iguana_adc"
  ];

  boot.extraModprobeConfig = ''
    options iguana_adc output_mask=0 enable_pullups=1
  '';

  services.udev.packages = with pkgs; [
    gs24dsi
  ];

  services.fixlockdir.enable = true;
}

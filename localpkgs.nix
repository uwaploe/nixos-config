{ config, lib, pkgs, ... }:

{
  nixpkgs.config = {
    packageOverrides = pkgs: {
      inherit (pkgs.callPackages ./pkgs/default.nix { })
        aiofxload aiousb-fw gs24dsi gs24dsi16wrc iguana-adc arms-utils;
    };
  };
}
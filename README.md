# NixOS Configuration for ARMS and IVAR

Custom NixOS configuration for the ARMS and IVAR System Interface
Controllers.

## Prerequisites

See the [NixOS Manual](https://nixos.org/nixos/manual/) for the basic
installation instructions.

+ A minimal NixOS installation with `git` installed
+ Use `fdisk` to partition the external "data" disk (`/dev/sdb`).
+ Create a filesystem on the external disk and label it (`$PRJ` is either
  "arms" or "ivar"):

``` shellsession
PRJ=arms # (or PRJ=ivar)
mkfs.ext4 -L ${PRJ}data /dev/sdb1
```

## Installation

``` shellsession
mv /etc/nixos /etc/nixos.dist
git clone https://bitbucket.org/uwaploe/nixos-config.git /etc/nixos
cp /etc/nixos.dist/hardware-configuration.nix /etc/nixos/
ln -sr /etc/nixos/config-${PRJ}.nix /etc/nixos/configuration.nix
nixos-rebuild switch
```

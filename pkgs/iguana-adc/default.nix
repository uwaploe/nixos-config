{ stdenv, fetchurl, kernel}:

let
  version = "1.6.1";
  repo = "uwaploe/arms-iguana-adc";
  tarball = "release-${version}.tar.gz";
  kd = "${kernel.dev}/lib/modules/${kernel.modDirVersion}";
in
stdenv.mkDerivation {
  name = "iguana_adc-${version}-${kernel.version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "1nm18kxw05bbm9zcg5q74lazimnisa99rh2gac7na6h6w6vxijp6";
  };

  hardeningDisable = [ "pic" ];

  buildPhase = ''
    make KERNEL_DIR=${kd}/build
  '';

  installPhase = ''
    binDir="$out/lib/modules/${kernel.modDirVersion}/misc"
    mkdir -p "$binDir"
    cp iguana_adc.ko "$binDir"
  '';

  meta = {
    description = "Kernel module driver for Versalogic Iguana A/D";
    homepage = "https://bitbucket.org/uwaploe/arms-iguana-adc";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}

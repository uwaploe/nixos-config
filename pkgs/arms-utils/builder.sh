# -*- mode: shell-script -*-
source $stdenv/setup

mkdir -p src
cd src
tar xvzf $src
cd *arms-utils*
mkdir -p $out/sbin $out/bin

cp -v lpmode.sh $out/sbin/lpmode
chmod a+x $out/sbin/lpmode
patchShebangs $out/sbin

cp -v lpsleep.sh $out/bin/lpsleep
chmod a+x $out/bin/lpsleep
patchShebangs $out/bin
for f in $out/bin/*; do
    wrapProgram "$f" --prefix PATH ':' "${binPath}"
done

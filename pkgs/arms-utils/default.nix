{ stdenv, fetchurl, coreutils, mosquitto, makeWrapper, kmod }:

let
  version = "0.7";
  repo = "uwaploe/arms-utils";
  tarball = "release-${version}.tar.gz";
in
stdenv.mkDerivation {
  name = "arms-utils-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "04hnbpjcxyynykcf3xks90ivmgi48mik6l3aav4q9lvn8m68a9bn";
  };

  buildInputs = [ makeWrapper ];
  binPath = stdenv.lib.makeBinPath [ coreutils mosquitto kmod ];
  builder = ./builder.sh;

  meta = {
    description = "Various utility programs for the ARMS project";
    homepage = "https://bitbucket.org/uwaploe/arms-utils";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}

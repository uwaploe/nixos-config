{ pkgs, ... }:

with pkgs;

{
  aiofxload = callPackage ./aiofxload/default.nix { };
  aiousb-fw = callPackage ./aiousb-fw/default.nix { };
  arms-utils = callPackage ./arms-utils/default.nix { };
  iguana-adc = callPackage ./iguana-adc/default.nix { kernel = linux; };
  gs24dsi = callPackage ./gs24dsi/default.nix {
    kernel = linux;
    bash = bash;
  };
  gs24dsi16wrc = callPackage ./gs24dsi16wrc/default.nix {
    kernel = linux;
    bash = bash;
  };

}
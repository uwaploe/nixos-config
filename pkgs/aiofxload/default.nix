{ stdenv, fetchgit, cmake }:

let
  version = "2019_01_17";
in
stdenv.mkDerivation {
  name = "aiofxload-${version}";
  src = fetchgit {
    url = "https://github.com/accesio/fxload.git";
    rev = "dc90a2333aae625d0b144ce438acf2edd23a5110";
    sha256 = "0c50pbm5vxi3nf6am0wc9wsbkmiimh7c6nslxkphs4a3di0xrapn";
  };
  buildInputs = [cmake];

  installPhase = ''
    mkdir -p $out/bin
    cp -v fxload $out/bin
    mkdir -p $out/share/man/man.8 $out/share/usb
    cp -v ../fxload.8 $out/share/man/man.8
    cp -v ../a3load.hex $out/share/usb
  '';

  meta = {
    description = "Tool to upload firmware to Cypress EZ-USB microcontrollers (AccesIO version)";
    homepage = "http://accesio.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}

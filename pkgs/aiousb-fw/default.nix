{ stdenv, fetchurl, aiofxload, perl, coreutils, bash }:

## Usage
# In NixOS, simply add this package to services.udev.packages:
#   services.udev.packages = [ pkgs.aiousb-fw ];

let
  version = "1.117.1";
in
stdenv.mkDerivation {
  name = "aiousb-fw-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/uwaploe/nixos-config/downloads/AIOUSB-${version}.tar.gz";
    sha256 = "01d9nh0maswawljc4bw9naqrfd35r9lxnzn4wgxv8a4jh0r0q49m";
  };

  inherit aiofxload perl coreutils bash;

  patches = [
    ./fw-load.patch
  ];

  builder = ./builder.sh;

  meta = {
    description = "Firmware for the Acces I/O USB data acquisition boards";
    homepage = "http://accesio.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}

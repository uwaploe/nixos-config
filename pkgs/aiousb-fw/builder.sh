# -*- mode: shell-script -*-
source $stdenv/setup

mkdir -p src
cd src
tar xzf $src

for d in *; do
  if [ -d "$d" ]; then
    cd "$d"
    break
  fi
done

cd ./AIOUSB
patchPhase
echo "Installing firmware files ..."
mkdir -p $out/share/usb
cp -v ./Firmware/*.hex $out/share/usb

echo "Installing udev rules ..."
mkdir -p $out/lib/udev/rules.d
cp -v ./Firmware/10-acces_usb.rules $out/lib/udev/rules.d
substituteInPlace $out/lib/udev/rules.d/10-acces_usb.rules \
        --replace /sbin/fxload $aiofxload/bin/fxload \
        --replace /usr/share $out/share \
        --replace /bin/chmod $coreutils/bin/chmod \
        --replace /bin/sleep $coreutils/bin/sleep \
        --replace /bin/sh $bash/bin/sh

echo "Installing firmware loader ..."
mkdir -p $out/bin
cp -v ./Firmware/accesloader.pl $out/bin
chmod +x $out/bin/accesloader.pl
substituteInPlace $out/bin/accesloader.pl \
                  --replace /usr/share $out/share \
                  --replace /usr/bin/perl $perl/bin/perl

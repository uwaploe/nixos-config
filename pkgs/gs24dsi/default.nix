{ stdenv, fetchurl, kernel, bash }:

let
  version = "4.3.65.13.0";
  repo = "uwaploe/gs24dsi";
  tarball = "v${version}.tar.gz";
  kd = "${kernel.dev}/lib/modules/${kernel.modDirVersion}";
in
stdenv.mkDerivation {
  name = "gs24dsi-${version}-${kernel.version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "0fg0vizizkq8my7i56dwdgni4j94mn9bjhvbdrq0xpv3cvilx6hk";
  };

  hardeningDisable = [ "pic" ];

  patches = [
    ./linux-4x.patch
  ];

  preBuild = ''
    cd ./driver
  '';

  buildPhase = ''
    cd ./driver
    make -C ${kd}/build M=$(pwd) GSC_DEV_DIR=$(pwd)
  '';

  installPhase = ''
    moddir="$out/lib/modules/${kernel.modDirVersion}/misc"
    mkdir -p $moddir
    cp 24dsi.ko $moddir
    mkdir -p $out/bin
    cp ${./init24dsi} $out/bin/init24dsi
    chmod a+x $out/bin/init24dsi
    substituteInPlace $out/bin/init24dsi \
        --replace /bin/bash ${bash}/bin/bash
    mkdir -p $out/lib/udev/rules.d
    cat > $out/lib/udev/rules.d/90-24dsi.rules <<-EOF
    SUBSYSTEM=="module", DEVPATH=="/module/24dsi", RUN+="$out/bin/init24dsi"
    EOF
  '';

  meta = {
    description = "Kernel module driver for GS 24DSI A/D boards";
    homepage = "http://www.generalstandards.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
